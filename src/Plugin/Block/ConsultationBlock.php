<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\XaiBlock.
 */
namespace Drupal\consultation\Plugin\Block;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Datetime\DrupalDateTime;
/**
 * Provides a 'Consultation' block.
 *
 * @Block(
 *   id = "consultation_block",
 *   admin_label = @Translation("Consultation block"),
 *   category = @Translation("Custom Consultation block")
 * )
 */
class ConsultationBlock extends BlockBase implements BlockPluginInterface{
  /**
   * {@inheritdoc}
   */
   

  public function blockForm($form, FormStateInterface $form_state) {
	// Form constructor.
    $form = parent::blockForm($form, $form_state);
    // Default settings.
    $config = $this->getConfiguration();
    $form['company_name'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Company name'),
      '#description' => $this->t('Add Company Name'),
      '#default_value' => isset($config['name']) ? $config['name'] : 'Consultation', 
    );

    $form['company_detail'] = array (
      '#type' => 'textarea',
      '#title' => $this->t('Company Detail'),
      '#description' => $this->t('Please add detail'),
      '#default_value' => isset($config['detail']) ? $config['detail'] : 'Informal call to review Site development capabilities and methodology.',
    );

	$form['image'] = array(
      '#type' => 'managed_file',
      '#upload_location' => 'public://images/',
      '#title' => $this->t('Image'),
      '#description' => t("Company Logo"),
      '#default_value' => isset($config['image']) ? $config['image'] : '',
      '#required' => TRUE,
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
        'file_validate_size' => array(25600000),
      ),
      '#states'        => array(
        'visible'      => array(
          ':input[name="image_type"]' => array('value' => t('Upload New Image(s)')),
        )
      )
    );

    return $form;
  }
  
    /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
  	parent::blockSubmit($form, $form_state);
    $this->setConfigurationValue('name', $form_state->getValue('company_name')); 
	$this->setConfigurationValue('detail', $form_state->getValue('company_detail')); 
	$image = $form_state->getValue('image');
	$this->configuration['image'] = $image;
	/* Load the object of the file by it's fid */
	$file = \Drupal\file\Entity\File::load( $image[0] );
	/* Set the status flag permanent of the file object */
	$file->setPermanent();
	/* Save the file in database */
	$file->save();
  }
  
 	public function build() {
		$config = $this->getConfiguration();
		$slot_config = \Drupal::config('consultation.adminsettings');
		$company_name = $this->t('Schedule A Free Consultation');
		$company_detail = $this->t('Informal call to review Site development capabilities and methodology.');
		$time = ($slot_config->get('allow_times') != '') ? $slot_config->get('allow_times') : '30';
		if (!empty($config['name']))
		  $company_name = $config['name'];
		
		if (!empty($config['detail']))
		  $company_detail = $config['detail'];

		$build = 'Picture';
		if (isset($this->configuration['image']) && !empty( $this->configuration['image'] )  ) {
			$image_field = $this->configuration['image'];
	    $image_uri =  \Drupal\file\Entity\File::load( $image_field[0] );
	    $build = $image_uri->uri->value;
	    $build = file_create_url($build); 
		}
		
	  $calendar = \Drupal::formBuilder()->getForm('Drupal\consultation\Form\ConsultationCalendarForm');
	  $webform = \Drupal::entityTypeManager()->getStorage('webform')->load('consultation');
	  $webform = $webform->getSubmissionForm();
	  return array('#markup' => $this->t('<div class="companydetail_block">
			<div class="summary-region">
			<div class="summary">
			<img alt="techseria" data-entity-type="file" data-entity-uuid="24202a92-7dac-4410-850f-3043b98135c9" src="@logo" />
			<h1 class="profile-info-event-type-name">@companyname Intro</h1>
			</div>

			<div class="details has-shrinkable-items is-mobile-hidden">
			<div class="details-item">@time min</div>
			</div>

			<div class="mbm">@companydetail</div>
			</div>
			</div></div>', 
			array (
			  '@companyname' => $company_name, '@companydetail' => $company_detail, '@logo' => $build,'@time' => $time,
		  )
			),$calendar,$webform);
  }

}