<?php

namespace Drupal\consultation\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

class DatetimeSettingForm extends ConfigFormBase {
	
	/**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return array(
      'consultation.adminsettings',
    );
  }

	
	
	
	/**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'datetime_setting_form';
  }
  
  
   /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
	 // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('consultation.adminsettings');  

    $form['hour_format'] = array(
      '#type' => 'select',
      '#title' => $this->t('Hours Format'),
      '#description' => $this->t('Select the hours format'),
      '#options' => [
        '12' => $this->t('12 Hours'),
        '24' => $this->t('24 Hours'),
      ],
      '#default_value' => $config->get('hour_format'),
      '#required' => TRUE,
    );
	$form['allow_times'] = array(
      '#type' => 'select',
      '#title' => $this->t('Minutes granularity'),
      '#description' => $this->t('Select granularity for minutes in calendar'),
      '#options' => [
        '10' => $this->t('10 minutes'),
        '15' => $this->t('15 minutes'),
        '30' => $this->t('30 minutes'),
        '60' => $this->t('60 minutes'),
      ],
      '#default_value' => $config->get('allow_times'),
      '#required' => TRUE,
    );
	$form['allowed_hours'] = array(
       '#type' => 'textfield',
      '#title' => $this->t('Allowed hours'),
      '#description' => $this->t('Specify allowed hours on time picker. Leave empty for no restrictions. Enter hours in following format of number, etc: 8,9,10,11,12,13,14,15,16,17. Separate by comma. This is used in combination with minutes granularity.'),
      '#default_value' => $config->get('allowed_hours'),
      '#required' => FALSE,
    );
	$form['disable_days'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Disable specific days in week'),
      '#description' => $this->t('Select days which are disabled in calendar, etc. weekends or just Friday'),
      '#options' => [
        '1' => $this->t('Monday'),
        '2' => $this->t('Tuesday'),
        '3' => $this->t('Wednesday'),
        '4' => $this->t('Thursday'),
        '5' => $this->t('Friday'),
        '6' => $this->t('Saturday'),
        '7' => $this->t('Sunday'),
      ],
      '#default_value' => $config->get('disable_days'),
      '#required' => FALSE,
    );
	$form['exclude_date'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Disable specific dates from calendar'),
      '#description' => $this->t('Enter days in following format d.m.Y etc. 31.12.2018. Each date in new line. This is used for specific dates, if you want to disable all weekends use settings above, not this field.'),
      '#default_value' => $config->get('exclude_date'),
      '#required' => FALSE,
    );
	$form['inline'] = array(
       '#type' => 'checkbox',
      '#title' => $this->t('Render inline'),
      '#description' => $this->t('Select if you want to render the widget inline.'),
      '#default_value' => 1,
      '#required' => FALSE,
    );
	$form['min_date'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Set a limit to the minimum date/time allowed to pick.'),
      '#description' => $this->t('Examples: \'0\' for now, \'+1970/01/02\' for tomorrow, \'12:00\' for time, \'13:45:34\',formatTime:\'H:i:s\'. <a href=":external">More info</a>',
        [':external' => 'https://xdsoft.net/jqplugins/datetimepicker/']),
      '#default_value' => $config->get('min_date'),
      '#required' => FALSE,
    );
	/*$form['max_date'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Set a limit to the max_date date/time allowed to pick.'),
      '#description' => $this->t('Examples: \'0\' for now, \'+1970/01/02\' for tomorrow, \'12:00\' for time, \'13:45:34\',formatTime:\'H:i:s\'. <a href=":external">More info</a>',
        [':external' => 'https://xdsoft.net/jqplugins/datetimepicker/']),
      '#default_value' => $config->get('max_date'),
      '#required' => FALSE,
    );*///maximum end date
	$form['max_time_weekend'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Set a time limit maxtime for Weekend.'),
      '#description' => $this->t('IF user need weekend End time Lasttime 04:00 you need to write time: 16:00'),
      '#default_value' => $config->get('max_time_weekend'),
      '#required' => FALSE,
    );
	$form['max_time_weekday'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Set a time limit maxtime for Weekdays.'),
      '#description' => $this->t('IF user need weekDays End time Lasttime 11:45 you need to write time: 23:45'),
      '#default_value' => $config->get('max_time_weekday'),
      '#required' => FALSE,
    );
	$form['year_start'] = array(
       '#type' => 'textfield',
      '#title' => $this->t('Start year'),
      '#description' => $this->t('Start value for fast Year selector - used only for selector'),
      '#default_value' => $config->get('year_start'),
      '#required' => FALSE,
    );
	$form['year_end'] = array(
       '#type' => 'textfield',
      '#title' => $this->t('End year'),
      '#description' => $this->t('End value for  Year selector - used only for selector'),
      '#default_value' => $config->get('year_end'),
      '#required' => FALSE,
    );
  $form['tsemail_image'] = array(
    '#type' => 'managed_file',
    '#title' => t('Upload Logo'),
    '#description' => t('Select a logo  for mail templete of at least @dimensionspx and maximum @filesize.', array(
      '@dimensions' => '600x240',
      '@filesize' => format_size(file_upload_max_size()),
    )),
    '#theme' => 'image_widget',
    '#preview_image_style' => 'medium',
    '#upload_location' => 'public://mail-logo',
    '#default_value' => $config->get('tsemail_image'),
    '#required' => TRUE,
  );
     return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	  parent::submitForm($form, $form_state);
	  
	  $this->config('consultation.adminsettings')
	  ->set('hour_format', $form_state->getValue('hour_format'))
	  ->set('allow_times', $form_state->getValue('allow_times'))
	  ->set('allowed_hours', $form_state->getValue('allowed_hours'))
	  ->set('disable_days', $form_state->getValue('disable_days'))
	  ->set('exclude_date', $form_state->getValue('exclude_date'))
	  ->set('inline', $form_state->getValue('inline'))
	  ->set('min_date', $form_state->getValue('min_date'))
	  ->set('max_date', $form_state->getValue('max_date'))
	  ->set('max_time_weekend', $form_state->getValue('max_time_weekend'))
	  ->set('max_time_weekday', $form_state->getValue('max_time_weekday'))
	  ->set('year_start', $form_state->getValue('year_start'))
	  ->set('year_end', $form_state->getValue('year_end'))
    ->set('tsemail_image', $form_state->getValue('tsemail_image'))
      ->save();
    
  }
 
  
}