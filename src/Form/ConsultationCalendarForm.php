<?php

namespace Drupal\consultation\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;


class ConsultationCalendarForm extends FormBase {
 
 /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'consultation_calendar_form';
  }
 /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
   $form['consultation_date'] = [
   '#type' => 'textfield',
   '#time' => TRUE,
   '#default_value' => drupal_get_user_timezone(),
   '#required' => False,
   '#attributes' => \Drupal\consultation\AttributeHelper::defaultWidget(),
   
 ];
 $form['#attached']['library'][] = 'consultation/datetimepicker';
 $form['#attributes']['autocomplete'] = 'off';

return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	
  }

}