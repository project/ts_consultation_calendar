<?php

namespace Drupal\consultation;

use Drupal\Component\Serialization\Json;

/**
 * Class AttributeHelper
 *
 * @package Drupal\consultation
 */
class AttributeHelper {

  /**
   * Attributes for textfield (or any non consultation field types).
   *
   * @return array
   *   Return attributes.
   */
   
    public static function defaultWidget() {
    $config = \Drupal::config('consultation.adminsettings');
    $allowed_hours = \Drupal::config('consultation.adminsettings')->get('allowed_hours');
	$min_date = \Drupal::config('consultation.adminsettings')->get('min_date');
	$inline = \Drupal::config('consultation.adminsettings')->get('inline');
    $array = explode(',', $allowed_hours);
    $first = $array [0];
	$last = end($array);
	return [
      'data-hour-format' => $config->get('hour_format'),
      'data-first-day' => \Drupal::config('system.date')->get('first_day'),
      'data-disable-days' => [$config->get('disable_days')],
      'data-allow-times' => $config->get('allow_times'),
      'data-allowed-hours' =>($allowed_hours == "") ? Json::encode(range(10,23)) : Json::encode(range($first,$last)),
      'data-inline' => ($inline == "") ? 1 : $config->get('inline'),
	  'data-min-date' => $config->get('min_date'),
      'data-exclude-date' => $config->get('exclude_date'),
      'data-year-start' => $config->get('year_start'),
	  'data-year-end' => $config->get('year_end'),
      'data-single-date-time' => 'datetime',
	  'data-max-time-weekend' => $config->get('max_time_weekend'),
	  'data-max-time-weekday' => $config->get('max_time_weekday'),
	  'data-current-date' => date('d'),
    ];
  }



  /**
   * Date only widget - for non consultation field types.
   *
   * @return array
   *   Return attributes.
   */
  public static function defaultDateOnlyWidget() {
    return self::defaultWidget() + ['data-single-date-time' => 'date'];
  }

  /**
   * List of all attributes - (for non consultation field types).
   *
   * @return array
   *   Return all attributes.
   */
  public static function allAtributes() {
    return [
      'data-hour-format' => 24,
      'data-first-day' => \Drupal::config('system.date')->get('first_day'),
      'data-disable-days' => [],
      'data-allow-times' => 30,
      'data-allowed-hours' => Json::encode(range(0, 23)),
      'data-inline' => '0',
      'data-mask' => FALSE,
      'data-datetimepicker-theme' => 'default',
      'data-single-date-time' => 'datetime',
      'data-exclude-date' => '',
      'data-min-date' => date('Y-m-d  H:i:s'),
      'data-max-date' => date('Y-m-d  H:i:s'),
      'data-year-start' => '1970',
      'data-year-end' => date('Y'),
      'data-current-date' => date('d'),

    ];
  }

  /**
   * All attributes for consultation field type.
   *
   * @return array
   *   Return formatted array.
   */
  public static function allElementAttributes() {
    return [
      '#hour_format' => 24,
      '#first_day' => \Drupal::config('system.date')->get('first_day'),
      '#disable_days' => [],
      '#allow_times' => 30,
      '#allowed_hours' => Json::encode(range(0, 23)),
      '#inline' => '0',
      '#mask' => FALSE,
      '#datetimepicker_theme' => 'default',
      '#single_date_time' => 'datetime',
      '#exclude_date' => '',
      '#min_date' => '',
      '#max_date' => '',
      '#year_start' => '1970',
      '#year_end' => date('Y'),
      '#data_current_date' => date('d'),
    ];
  }

}
