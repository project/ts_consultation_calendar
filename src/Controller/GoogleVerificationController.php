<?php
/**
 * @file
 * Contains \Drupal\consultation\Controller\ConsultationController.
 */

namespace Drupal\consultation\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
require __DIR__ . '/../../vendor/autoload.php';
use Google_Client;
use Google_Service_Calendar;

/**
 * An example controller.
 */
class GoogleVerificationController extends ControllerBase
{

    /**
     * Returns a render-able array for a test page.
     */
    public function content()
    {
        $code = \Drupal::request()->query->get('code');
        $client = new Google_Client();
        $client->setApplicationName('Techseria app');
        $client->setScopes(Google_Service_Calendar::CALENDAR);
        $client->setAuthConfig(__DIR__ . '/../../credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $accessToken = $client->authenticate($code);
        $tokenPath = __DIR__ . '/../../token.json';
        if (!file_exists($tokenPath))
        {
            echo mkdir(dirname($tokenPath) , 0700, true);
        }
        file_put_contents($tokenPath, json_encode($accessToken));
        $client->setAccessToken($accessToken["access_token"]);
        $tempstore = \Drupal::service('tempstore.private');
        $store = $tempstore->get('consultation_collection');
        $form_state = $store->get('form_state');
        eventcreate($client, $form_state);
        return array(
            "#type" => "markup",
            '#markup' => '<div class="thankyou-heading">' . $this->t('Thank You For Join Informal call') . '</div>',
            "value" => $client
        );
    }

}

