<?php
/**
 * @file
 * Contains \Drupal\consultation\Controller\ConsultationController.
 */

namespace Drupal\consultation\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Datetime\DrupalDateTime;
/**
 * Class ConsultationController
 *
 * @package Drupal\consultation\Controller
 */
class ConsultationController extends ControllerBase{

 	public  function get_bookings_date(Request $request){
      
    $current_date = $request->request->get('current_date');
    $allowed_hours = \Drupal::config('consultation.adminsettings')->get('allowed_hours');
	$allow_times = (\Drupal::config('consultation.adminsettings')->get('allow_times') != '') ? \Drupal::config('consultation.adminsettings')->get('allow_times') : '30';
	if(empty($allowed_hours))
		{
				$allowed_hours = [10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		}
    else
	    {
				$allowed_hours = explode(',',$allowed_hours);
		}
       $allowed_hours_array_list = [];
	   foreach ($allowed_hours as &$value)
	   {
			if ($value < 10) {
			   $value = '0'.$value;
			}
			$seconds = 00 ;
			$time = '';
			while($seconds < 60)
			{
				if($seconds == 60){
					
				   $valuesum = $value+1;
				   $time = $valuesum. ':00';
				}
				else
				{
					if ($seconds == 0) {
						$time = $value. ':00';
					}
					else
					{
						 $time = "$value:$seconds";
					}
				}
				$seconds = $seconds + $allow_times;
				$allowed_hours_array_list[] = $time;
			}
	     }
	$database = \Drupal::database();
    $query = $database->select('webform_submission_data', 'wsd');
    $query->innerJoin('webform_submission_data', 'wsd_time_sloat', 'wsd_time_sloat.sid = wsd.sid');
    $query->fields('wsd', ['sid', 'value','name']);
    $query->fields('wsd_time_sloat', ['sid', 'value','name']);
    $query->condition('wsd.value',$current_date);
    $query->condition('wsd_time_sloat.name','slot_detail');
    $result = $query->execute()->fetchAll(); 
		if(!empty($result)){
      $time_sloat_array = array_map(function($arg){
	      $unix_time =  strtotime($arg->wsd_time_sloat_value);
	      $time_sloat = date('H:i',$unix_time);
        return $time_sloat;
     	}, $result);
    	$response['data']  = $result;
    	$response['allow_time_sloat'] =  array_diff($allowed_hours_array_list,$time_sloat_array);
   	} else {
      $response['data']  = null;
      $response['allow_time_sloat'] =  $allowed_hours_array_list;
   	}     
    return new JsonResponse( $response );
   }
}
