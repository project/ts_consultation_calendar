/**
 * @file
 * README file for Consultation Booking 
 */
 
CONTENTS
--------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Notices


# Introduction

Consultation makes it easy for customers to schedule appointments with you, which is exactly the kind of functionality that's nice to have on a website.
you set up an event type (say a 30 minute Zoom call) + add in your preferred availability, email reminders, form questions, etc.
Consultation Module will look at your calendars to see what your availability is or isn’t. It will show available meeting times during the blocks that you already set to anyone with your link.

## REQUIREMENTS

This module requires the following module and libraries:

 * Webform - https://www.drupal.org/project/webform  (Module)
 * Datetimepicker (library)
 
```bash
Download Source code on GitHub(https://github.com/xdan/datetimepicker)
```
OR

or download [zip](https://github.com/xdan/datetimepicker)

Connect event with Google Calendar Flow steps

Install the Google Client Library (Consultation root module and install composer)
```
composer require google/apiclient:^2.0
```
-- Enable the Google Calendar API
	 1. [https://console.cloud.google.com/](https://console.cloud.google.com/)
	 2. Create  New Project (i.e google calendar event)
	 3. Open menu and Navigate to APIs & Services>OAuth consent screen and          create page and Select API For google verification(calendar,calendar.events,calendar.events.readonly,calendar.readonly,calendar.settings.readonly)
	 4. Add Authorised domain name
	 5. Add Application Homepage link
	 6. Add Application Privacy Policy link
	 7. Application Terms of Service link
	 8. After verification Navigate to APIs & Services >Credentials and  create  OAuth 2.0 Client IDs  for top side  click "Create credentials" and 
	 9. select OAuth client ID select Web application > add your app name add Authorised JavaScript origins name (I.e https://abc.com) then add Authorised redirect URIs(i.e https://abc.com/googlecode)
	 10.**DOWNLOAD CLIENT CONFIGURATION** and save the file `credentials.json` to consultation module directory.
	 
## INSTALLATION

Install the module and enable it according to Drupal standards.

The module's configuration pages reside at:
- admin/config/consultation/settings

Install Library in \libraries folder name must be "datetimepicker"  from  datetimepicker-master




## CONFIGURATION

   1. Navigate to Administration > Extend and enable the Consultation module.
   2. Configure the module at admin/config/consultation/settings - add all calendar requirements
   3. Navigate to Administration >Structure>Block layout and select any region and click place block and select (Consultation block) Block name and on select block open block insert your detail (i.e company name,company Detail,logo image) save

## NOTICES
For token verification you need to add compulsory add "googlecode" on Authorised redirect URIs (i.e https://abc.com/googlecode)

## Troubleshooting

- Ensure the `credentials.json` to  added your working directory.
- Ensure the Library to added your library directory.